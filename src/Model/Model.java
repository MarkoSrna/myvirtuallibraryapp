package Model;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Model {
    private Connection myConnection;

    public Connection getConnection() {
        //Get Model properties
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("mylib.properties"));

            String user = properties.getProperty("user");
            String password = properties.getProperty("password");
            String dburl = properties.getProperty("dburl");

            //Connect to DB
            myConnection = DriverManager.getConnection(dburl, user, password);

            System.out.println("Successfully connected to " + dburl + "Model");

        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("FAILED TO READ CONNECTION PROPERTIES!");
        } catch (SQLException e) {
            e.printStackTrace();
            System.err.println("SQL EXCEPTION CONNECTION FAILED!");
        }
        return myConnection;
    }
}


