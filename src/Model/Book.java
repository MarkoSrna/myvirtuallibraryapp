package Model;

public class Book {

    private int iD;
    private String title;
    private int length;
    private int original_In_ID;

    public Book(int iD, String title, int length, int original_In_ID) {
        super();
        this.iD = iD;
        this.title = title;
        this.length = length;
        this.original_In_ID = original_In_ID;
    }

    public void setID() {
        this.iD = iD;
    }

    public void setTitle() {
        this.title = title;
    }

    public void setLength() {
        this.length = length;
    }

    public void setOriginal_In_ID() {
        this.original_In_ID = original_In_ID;
    }


    public int getID() {
        return iD;
    }

    public String getTitle() {
        return title;
    }

    public int getLength() {
        return length;
    }

    public int getOriginal_In_ID() {
        return original_In_ID;
    }
}

