package Model;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class BookDAO {

    public void addBook(Book book) {
        System.out.println("New Book Added");
    }

    public Book getBook(String title) {
        return null;
    }

    public List<Book> getBooks(){
        return null;
    }
    public void updateBook(Book book){

    }

    public void deleteBook(int iD) {

    }

    private Book convertRowToBook(ResultSet myRs) throws SQLException {

        int id = myRs.getInt("id");
        String title = myRs.getString("title");
        int length = myRs.getInt("length");
        int original_In_ID= myRs.getInt("original_In_ID");

        Book tempBook = new Book(id, title, length, original_In_ID);

        return tempBook;
    }

}
